import math
import random
import numpy as np

x0 = 0
y0 = 0

target = 100

class cannon:
    # def __init__(self,x,y,v,unghi):
    #     self.x = x
    #     self.y = y
    #     self.vx = v*math.cos(math.radians(unghi))

    def __init__(self, unghi, velocity):
        self.unghi = unghi
        self.velocity = velocity
        self.viteza = velocity * math.cos(math.degrees(self.unghi))

def starting_pop (numar_indivizi):
    population = np.zeros((numar_indivizi,2))
    for i in range (numar_indivizi):
        chrome = cannon(random.randint(0,90),random.randint(0,100))
        population[i][0] = chrome.unghi
        population[i][1] = chrome.viteza
    #print(population)
    # print ("Forta este:",chrome.forta, "unghiul este",chrome.unghi)
    return population


def calculate_fitness(target, chromozom):
    fitness_score = 0
    distanta = target-chromozom[1]
    if (distanta > 0):
        fitness_score = distanta
        print("suntem la: ",distanta, "metrii distanta de tinta")
    if (distanta == 0):
        fitness_score = distanta
        print("Just hit the target")
# daca folosesc treaba asta de mai sus, un fitness score mai mic
# va fi mai bun decat unul mai mare, momentan treaba ramane asa
    return fitness_score

def select_individ (population, k):
    # cannon_1 = random.randint(0, len(population)-1)
    # cannon_2 = random.randint(0, len(population)-1)
    # cannon_1_fitness  = calculate_fitness(target,population[cannon_1])
    # cannon_2_fitness = calculate_fitness(target,population[cannon_2])
    # if cannon_1_fitness <= cannon_2_fitness:
    #     winner = cannon_1
    # else:
    #     winner = cannon_2
    # return population[winner,:]
    # cannon1 = random.randint(0,len(population) - 1)
    # best = None
    # for i in range (len(population)):
    #     cannon1 = random.randint(0, len(population) - 1)
    #     cannon1_fitness = calculate_fitness(target,population[cannon1])
    #     if best < cannon1_fitness:
    #         best = cannon1
    # return population[best,:]
    aspirants = []
    fitness = []
    for i in range (k):
        aspirants.append(random.choice(population))
        fitness.append(calculate_fitness(target, aspirants[i]))

    #     # print("k ",k)
    # print("len ", len(aspirants))
    #print('aspirants ', aspirants)
    #print('fitness ', fitness)
    index = fitness.index(min(fitness))
    return aspirants[index]
    # chosen = calculate_fitness(target, aspirants[0])
    # for j in range(len(aspirants)):
    #     # print("j ",j)
    #     fitness.append(calculate_fitness(target, aspirants[j]))


def worst_fitness (population):

    w_fitness = []

    print('population ', population)

    for i in population:
        w_fitness.append(calculate_fitness(target,i))
    index = w_fitness.index(max(w_fitness))
    return population[index]





def random_mutate (population, mutation_prob):
    for individual in range(len(population)):
        x = random.uniform(0,0.1)
        if x < mutation_prob :
            population[individual][0] = ((population[individual][0])-0.2) * 1.2
            population[individual][1] = (population[individual][1])-1
    return population

def breed_by_crossover (parent_1, parent_2):
    new_u = (parent_1[0] + parent_2[0])/2
    new_v = (parent_1[1]+parent_2[1])/2

    child_1 = [new_u,new_v]

    # new_u_2 = parent_1[0]*0.7 + parent_2[0]*0.3
    # new_v_2 = parent_1[1]*0.125 + parent_2[1]*0.975
    #
    # child_2 = [new_u_2,new_v_2]

    return child_1


c_length = 2
pop_size = 100
max_gen = 1000
number_of_individuals = 5


population = starting_pop(pop_size)


print("random choise: ",random.choice(population))

print("Populatia initiala este: ", population)

for generation in range(max_gen):

    # new_pop = []
    #
    # for i in range (int(pop_size)):

    parent_1 = select_individ(population,number_of_individuals)
    parent_2 = select_individ(population,number_of_individuals)
    child_1 = breed_by_crossover(parent_1,parent_2)
        # new_pop.append(child_1)
        # new_pop.append(child_2)
        # np_child = np.array(child_1)
    deleted_one = worst_fitness(population)
        # new_pop = np.delete(population, deleted_one)
        # new_pop = np.hstack((new_pop,child_1))
    population = np.delete(population, deleted_one)
    population = np.insert(population, population.size, child_1)
    # population = np.array(new_pop)s

    mutation_rate = 0.01
    population = random_mutate(population, mutation_rate)







